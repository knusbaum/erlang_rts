-module(fileserv).
-behavior(application).
-export([start/2, stop/1, 
         send_file/2, send_file/3, send_file/4,
         list_files/1, list_files/2, list_files/3]).

start(normal, _Args) ->
    supervisor:start_link(fileserv_supervisor, []).
%    fileserv_serv:start_link().

stop(_) ->
    ok.

send_file(Filename, Target) ->
    fileserv_client:send_file(Filename, Target).

send_file(Filename, Target, Hostname) ->
    fileserv_client:send_file(Filename, Target, Hostname).

send_file(Filename, Target, Hostname, Port) ->
    fileserv_client:send_file(Filename, Target, Hostname, Port).


list_files(Path) ->
    fileserv_client:list_files(Path).

list_files(Path, Hostname) ->
    fileserv_client:list_files(Path, Hostname).

list_files(Path, Hostname, Port) ->
    fileserv_client:list_files(Path, Hostname, Port).
