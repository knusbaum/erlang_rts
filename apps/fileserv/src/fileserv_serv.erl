-module(fileserv_serv).
-behavior(gen_server).
-include("Files.hrl").
-export([start_link/0, init/1, handle_call/3, handle_cast/2,
         handle_info/2, terminate/2, code_change/3, accept/1, handle_conn/1,
        do_list_dir/2]).

start_link() ->
    gen_server:start_link({local, fileserv}, ?MODULE, [], []).

init([]) ->
    process_flag(trap_exit, true),
    ok = erlogger:start_logger(fileserv_log, "fileserv.log"),
    {ok, Port} = application:get_env(port),
    {ok, LSock} = gen_tcp:listen(Port, [binary, 
                                        {active, false}, 
                                        {linger, {true, 5000}}]),
    Accept = spawn_link(?MODULE, accept, [LSock]),
    {ok, {LSock, Accept}}.
                             

accept(LSock) ->
    erlogger:log(fileserv_log, "Starting Accept loop.", []),
    {ok, Sock} = gen_tcp:accept(LSock),
    Pid = spawn_link(?MODULE, handle_conn, [Sock]),
    gen_tcp:controlling_process(Sock, Pid),
    accept(LSock).

handle_call(_, _From, {LSock, Accept}) ->
    erlogger:log(fileserv_log, "Handling call.", []),
    {reply, ok, {LSock, Accept}}.

handle_cast(_, {LSock, Accept}) ->
    {noreply, {LSock, Accept}}.

handle_info({'EXIT', Pid, Reason}, {LSock, Accept}) ->
    if Pid == Accept -> 
            erlogger:log(fileserv_log, "Accepter died. Launching new one.", []),
            NewAccept = spawn_link(?MODULE, accept, [LSock]),
            {noreply, {LSock, NewAccept}};
       true ->
            erlogger:log(fileserv_log, "File Server got shutdown: [Reason: ~p, From: ~p]",
                       [Reason, Pid]),
            {stop, Reason, {LSock, Accept}}
    end;
handle_info(Message, State) ->
    erlogger:log(fileserv_log, "Handling Info: ~p", [Message]),
    {noreply, State}.

terminate(Reason, {LSock, _Accept}) ->
    erlogger:log(fileserv_log, "Shutting down fileserv: ~p", [Reason]),
    gen_tcp:close(LSock),
    erlogger:stop_logger(fileserv_log),
    ok.

code_change(_, State, _) ->
    {ok, State}.


%% Setup the connection. Catch any exceptions and send an error ack
handle_conn(Sock) ->
    try 
%        do_transfer(Sock, [])
        dispatch(Sock)
    catch
        {error, tcp_closed} ->
                       erlogger:log(fileserv_log, "Failed to transfer. Connection closed by client.", []),
                       %% Don't throw. Just exit 
                       %% (the socket is closed, so we can't send a nack.)
                       {error, tcp_closed};
        Any -> erlogger:log(fileserv_log, "Failed to transfer: ~p.", [Any]),
               {ok, ErrAck} = 'Files':encode('Ack', #'Ack'{flag = error}),
               R = gen_tcp:send(Sock, ErrAck),
               erlogger:log(fileserv_log, "Tried to send nack: ~p.", [R]),
               gen_tcp:close(Sock),
               Any
    end.

dispatch(Sock) ->
    inet:setopts(Sock, [{active, true}]),
    receive
        {tcp, _Port, D} ->
            case 'Files':decode('Command', [D]) of
                {ok, Command} ->
                    case Command#'Command'.type of
                        send ->
                            {ok, Ack} = 'Files':encode('Ack', #'Ack'{flag = ready}),
                            gen_tcp:send(Sock, Ack),
                            do_send(Sock, []);
                        list ->
                            {ok, Ack} = 'Files':encode('Ack', #'Ack'{flag = ready}),
                            gen_tcp:send(Sock, Ack),
                            do_list(Sock);
                        _ ->
                            throw({error, {unimplemented, Command#'Command'.type}})
                    end;
                {error, Reason} ->
                    throw({error, Reason})
            end;
        {tcp_closed, _Port} ->
            throw({error, tcp_closed})
    after 10000 ->
            erlogger:log(fileserv_log, "Timed out waiting for Command.", []),
            throw({error, timeout})
    end.

getroot() ->
    case application:get_env(rootdir) of
        undefined -> "./";
        {ok, Other} -> Other
    end.

%%% Send Functions %%%

%% Do the transfer: Wait for the file message and then begin receiving.
do_send(Sock, Data) ->
    %inet:setopts(Sock, [{active, true}]),
    receive
        {tcp, _Port, D} -> 
            case 'Files':decode('File', [D | Data]) of
                {ok, File} ->
                    erlogger:log(fileserv_log, "Preparing to transfer file: ~p", [File]),
                    do_receive(Sock, File);
                {error, Reason} ->
                    throw({error, Reason})
            end;
        {tcp_closed, _Port} ->
            throw({error, tcp_closed})
    after 10000 -> 
            erlogger:log(fileserv_log, "Timed out waiting for File spec.", []),
            throw({error, timeout})
    end.

%% Try to open the file, start the receive loop.
do_receive(Port, File) ->
    Root = getroot(),
    Filename = Root ++ File#'File'.filename,
    Length = File#'File'.filelen,
    erlogger:log(fileserv_log, "Beginning Transfer of ~p.", [Filename]),
    case file:open(Filename, [write]) of
        {ok, Handle} -> 
            {ok, Ack} = 'Files':encode('Ack', #'Ack'{flag = ready}),
            try 
                gen_tcp:send(Port, Ack),
                receive_loop(Port, Handle, Length, 0)
            catch
                Any -> erlogger:log(fileserv_log, "Failure. Removing partial file.", []),
                       file:delete(Filename),
                       throw(Any)
            end;
        Res -> erlogger:log(fileserv_log, "Failed to open file: ~p.", [Filename]),
               throw(Res)
    end.

%% loop, reading file data until we have all the bytes specified in the 
%% File message we received.
receive_loop(Port, FileHandle, Length, Read) ->
    receive
        {tcp, Port, D} ->
            DLen = byte_size(D),
            file:write(FileHandle, D),
            if 
                DLen + Read < Length ->
                    receive_loop(Port, FileHandle, Length, DLen + Read); 
                true -> 
                    erlogger:log(fileserv_log, "Sucessful transfer.", []),
                    {ok, Ack} = 'Files':encode('Ack', #'Ack'{flag = done}),
                    gen_tcp:send(Port, Ack),
                    exit(normal)
            end;
        {tcp_closed, Port} ->
            throw({error, tcp_closed});
        Err -> erlogger:log(fileserv_log, "Failed to transfer: ", [Err]),
               throw({error, Err})
    after 5000 -> 
            erlogger:log(fileserv_log, "Timed out waiting for data. Got ~p/~p", [Read, Length]),
            throw({error, timeout})
    end.


%%% List Functions %%%

%% Do the list
do_list(Sock) ->
    receive
        {tcp, _Port, D} -> 
            case 'Files':decode('File', [D]) of
                {ok, File} ->
                    erlogger:log(fileserv_log, "Preparing to list dir: ~p", [File]),
                    do_list_dir(Sock, File);
                {error, Reason} ->
                    throw({error, Reason})
            end;
        {tcp_closed, _Port} ->
            throw({error, tcp_closed})
    after 10000 -> 
            erlogger:log(fileserv_log, "Timed out waiting for File spec.", []),
            throw({error, timeout})
    end.

do_list_dir(Sock, File) ->
    inet:setopts(Sock, [{active, false}]),

    Root = getroot(),
    Dirname = Root ++ File#'File'.filename,
    erlogger:log(fileserv_log, "About to list ~p.", [Dirname]),
    
    {ok, Filenames} = file:list_dir_all(Dirname),
    {ok, Files} = 'Files':encode('Files', #'Files'{files = Filenames}),
    gen_tcp:send(Sock,Files),
    
    {ok, Dat} = gen_tcp:recv(Sock, 0),
    ok = fileserv_common:get_ack(Dat, done),
    exit(normal).
