-module(fileserv_common).
-include("Files.hrl").
-export([get_ack/2]).

get_ack(Dat, Val) ->
    case 'Files':decode('Ack', Dat) of
        {ok, Ack} ->
            case Ack#'Ack'.flag of
                Val -> ok;
                _ -> throw({error, {unexpected_ack, Ack#'Ack'.flag}})
            end;
        Err -> throw(Err)
    end.
