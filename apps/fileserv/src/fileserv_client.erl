-module(fileserv_client).
-include("Files.hrl").
-export([send_file/2, send_file/3, send_file/4,
         list_files/1, list_files/2, list_files/3]).
-include_lib("kernel/include/file.hrl").

%% Try to send a file, catch any errors that might arise 
%% and return them.
send_file(Filename, Target) ->
    send_file(Filename, Target, "localhost").

send_file(Filename, Target, Hostname) ->
    {ok, Port} = application:get_env(fileserv, port),
    send_file(Filename, Target, Hostname, Port).

send_file(Filename, Target, Hostname, Port) ->
    try 
        safe_send(Filename, Target, Hostname, Port)
    catch
        {error, Reason} -> {error, Reason}
    end.

list_files(Path) ->
    list_files(Path, "localhost").

list_files(Path, Hostname) ->
    {ok, Port} = application:get_env(fileserv, port),
    list_files(Path, Hostname, Port).

list_files(Path, Hostname, Port) ->
    Sock = get_sock(Hostname, Port),

    % Tell the server we want to list a directory.
    C = #'Command'{type=list},
    {ok, CEnc} = 'Files':encode('Command', C),
    gen_tcp:send(Sock, CEnc),
    
    % Wait for server ready.
    {ok, Dat} = gen_tcp:recv(Sock, 0),
    ok = fileserv_common:get_ack(Dat, ready),
    
    % Send the directory.
    Dir = #'File'{filename=Path, filelen=0},
    {ok, DEnc} = 'Files':encode('File', Dir),
    gen_tcp:send(Sock, DEnc),
    
    % Get the response.
    {ok, FEnc} = gen_tcp:recv(Sock, 0),
    {ok, Files} = 'Files':decode('Files', [FEnc]),
    
    % Send ack
    {ok, Ack} = 'Files':encode('Ack', #'Ack'{flag = done}),
    gen_tcp:send(Sock, Ack),
    Files.


%% 'Safely' send a file. Get the size and send a File message.
%% If the server responds with a 'ready' ack, do_send(...)
safe_send(Filename, Target, Hostname, Port) ->
    
    Size = get_filesize(Filename),
    FileHandle = get_handle(Filename),
    Sock = get_sock(Hostname, Port),

    % Give the 'send' command.
    C = #'Command'{type=send},
    {ok, CEnc} = 'Files':encode('Command', C),
    gen_tcp:send(Sock, CEnc),
    {ok, Dat} = gen_tcp:recv(Sock, 0),
    ok = fileserv_common:get_ack(Dat, ready),
    
    % Send the file.
    F = #'File'{filename=Target, filelen=Size},
    {ok, FEnc} = 'Files':encode('File', F),
    gen_tcp:send(Sock, FEnc),
    {ok, Dat} = gen_tcp:recv(Sock, 0),
    ok = fileserv_common:get_ack(Dat, ready),
            
    do_send(FileHandle, Sock).

%% Do the send. Read the file in 1MiB chunks and send it.
%% The recv(...) at the beginning is in case the server
%% borks during the send, we can know about it and exit
%% early, so we don't waste time sending the file.
%% Finally, when we're done sending, we'll call get_done(...)
do_send(FileHandle, Sock) ->
    case gen_tcp:recv(Sock, 0, 0) of
        %% Timeout is ok. We didn't get any messages from the server.
        {error, timeout} -> ok;
        {error, Reason} -> throw({error, Reason});
        {ok, Data} ->
            case 'Files':decode('Ack', Data) of
                {error, Reason2} -> throw({error, Reason2});
                {ok, Ack} -> throw({error, {server, Ack#'Ack'.flag}})
            end
    end,

    case file:read(FileHandle, 20 * 1024 * 1024) of
        eof -> file:close(FileHandle),
               get_done(Sock),
               gen_tcp:close(Sock),
               ok;
        {error, Reason3} -> throw({error, Reason3});
        {ok, Data2} -> 
            gen_tcp:send(Sock, Data2),
            do_send(FileHandle, Sock)
    end.

%% Wait for confirmation from the server that everything went
%% alright. If the server crashed and couldn't send a 'nack', 
%% or if it didn't get the expected data, we'll find out here.
get_done(Sock) ->
    io:fwrite("Preparing to recieve 'done'~n"),
    case gen_tcp:recv(Sock, 0, 10000) of
        {error, Reason} -> throw({error, Reason});
        {ok, Data} ->
            case 'Files':decode('Ack', Data) of
                {error, Reason} -> throw({error, Reason});
                {ok, Ack} -> 
                    if 
                        Ack#'Ack'.flag == done -> ok;
                        true -> throw({error, {server, Ack#'Ack'.flag}})
                    end
            end
    end.


%%% Helpers %%%
%%% These are wrappers that turn error returns into throws.
%%% The thrown errors will be caught by send_file(...) and returned.
get_filesize(Filename) ->
    case file:read_file_info(Filename) of
        {error, Reason} -> throw({error, Reason});
        {ok, FileInfo} -> FileInfo#file_info.size
    end.
            
get_handle(Filename) ->
    case file:open(Filename, [read]) of
        {error, Reason} -> throw({error, Reason});
        {ok, Handle} -> Handle
    end. 

get_sock(Hostname, Port) ->
    case gen_tcp:connect(Hostname, Port, [{active, false}]) of
        {error, Reason} -> throw({error, Reason});
        {ok, Sock} -> Sock
    end.
