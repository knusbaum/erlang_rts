%%%-------------------------------------------------------------------
%% @doc kv2 top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(kv2_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    Nodes = case application:get_env(kv2, nodes) of
                undefined -> [node()];
                {ok, NodeList} ->
                    sets:to_list(sets:from_list([node() | NodeList]))
            end,
    {ok, {#{strategy => one_for_all,
            intensity => 1,
            period => 10},

          [#{id => kv2_worker_sup,
            start => {kv2_worker_sup, start_link, []},
            restart => permanent,
            shutdown => 5000,
            type => worker,
            modules => [kv2_worker_sup]},

           #{id => kv2_server,
             start => {kv2_server, start_link, [Nodes]},
             restart => permanent,
             shutdown => 5000,
             type => worker,
             modules => [kv2_server]}]}}.


%%====================================================================
%% Internal functions
%%====================================================================
