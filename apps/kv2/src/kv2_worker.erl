-module(kv2_worker).
-behavior(gen_server).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-define(SERVER, ?MODULE).

-define(BACKUP_COUNT, 5).

-include("kv2_common.hrl").

-record(state, {id = -1,
                workers=#{},
                kvs=#{},
                backup=#{}}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    gen_server:start_link(?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok,State}
%%                   | {ok,State,Timeout}
%%                   | {ok,State,hibernate}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    process_flag(trap_exit, true),
    erlogger:log(kv2_log, "Starting up kv2 worker: ~p", [self()]),
    gen_server:cast(self(), subscribe),
    {ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply,Reply,NewState}
%%                                 | {reply,Reply,NewState,Timeout}
%%                                 | {reply,Reply,NewState,hibernate}
%%                                 | {noreply,NewState}
%%                                 | {noreply,NewState,Timeout}
%%                                 | {noreply,NewState,hibernate}
%%                                 | {stop,Reason,Reply,NewState}
%%                                 | {stop,Reason,NewState}
%% @end
%%--------------------------------------------------------------------
handle_call(get_workers, _From, State=#state{workers=Workers}) ->
    {reply, Workers, State};
handle_call(Request, _From, State) ->
    erlogger:log(kv2_log, "Worker got call: ~p", [Request]),
    {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) ->
%%                             {noreply,NewState}
%%                           | {noreply,NewState,Timeout}
%%                           | {noreply,NewState,hibernate}
%%                           | {stop,Reason,NewState}
%% @end
%%--------------------------------------------------------------------
handle_cast({insert, Key, Value, From}, State) ->
    do_insert(Key, Value, From, State);
handle_cast({backup, Key, Value, Num}, State) ->
    do_backup(Key, Value, Num, State);
handle_cast({lookup, Key, From}, State) ->
    do_lookup(Key, From, State);
handle_cast({backup_lookup, Key, From}, State) ->
    do_backup_lookup(Key, From, State);
handle_cast(subscribe, State=#state{id=CurId, workers=Workers}) ->
    ID =  if CurId == -1 ->
                  uniform_excluding(?RING_SIZE, maps:keys(Workers));
             true -> CurId
          end,
    Subscribe = gen_leader:leader_call(kv2_server, {subscribe, ID, self()}),
    case Subscribe of
        {ok, ReplyWorkers} ->
            erlogger:log(kv2_log, "Successfully Subscribed."),
            {noreply, State#state{id=ID,
                                  workers=ReplyWorkers}};
        {id_in_use, ReplyWorkers} ->
            gen_server:cast(self(), subscribe),
            {noreply, State#state{id=-1, workers=ReplyWorkers}};
        _ -> erlogger:log(kv2_log, "Failed to subscribe: ~p",
                        [Subscribe]),
             {stop, Subscribe, State}
    end;
handle_cast({join, NewWorkers}, State=#state{id=ID, kvs=KVs}) ->
    %logger:log(kv2_log, "Worker: ~p[~p] join: ~p",
    %           [self(), ID, NewWorkers]),
    NewState = State#state{workers=NewWorkers},
    IDs = lists:sort(maps:keys(filter_live(NewWorkers))),
    % Redistribute primary kv pairs
    erlogger:log(kv2_log, "Shifting primary keys."),
    IStates =
        maps:values(
          maps:map(fun(K, V) ->
%                     case get_nth(K, 1, IDs) of
%                         ID -> ok;
%                         _ -> do_insert(K, V, none, NewState)
%                     end
                           {noreply, IState} = do_insert(K, V, none, NewState),
                           IState
                   end,
                   KVs)),
    MergedState=#state{backup=Backup} = 
        lists:foldl(fun(S=#state{backup=B1}, Acc=#state{backup=B2}) ->
                            Acc#state{backup=maps:merge(B1, B2)}
                    end,
                    NewState,
                    IStates),
    Filtered = maps:filter(fun(K, V) ->
                                   case get_nth(K, 1, IDs) of
                                       ID -> true;
                                       _ -> false
                                   end
                           end,
                           KVs),
    % drop invalid backups
%    erlogger:log(kv2_log, "Dropping backups."),
%    NewBackup = maps:filter(fun(K, {Num, Val}) ->
%                                    case get_nth(K, Num, IDs) of
%                                        ID -> true;
%                                        _  ->
%                                            erlogger:log(kv2_log, "Dropping backup ~p for ~p:~p", [Num, K, Val]),
%                                            false
%                                    end
%                            end,
%                            Backup),

    {noreply, MergedState#state{kvs=Filtered}};
handle_cast({part, NewWorkers}, State) ->
    %logger:log(kv2_log, "Worker: ~p[~p] part: ~p",
    %           [self(), ID, NewWorkers]),
    {noreply, State#state{workers=NewWorkers}};
handle_cast(Msg, State) ->
    erlogger:log(kv2_log, "Worker got cast: ~p", [Msg]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) ->
%%                             {noreply,NewState} | {noreply,NewState,Timeout}
%%                           | {noreply,NewState,hibernate}
%%                           | {stop,Reason,NewState}
%% @end
%%--------------------------------------------------------------------
handle_info({'EXIT', _Pid, Reason}, State) ->
    erlogger:log(kv2_log, "Worker got exit: ~p", [Reason]),
    {stop, Reason, State};
handle_info(Message, State) ->
    erlogger:log(kv2_log, "Worker got Info: ~p", [Message]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(Reason, _State) ->
    erlogger:log(kv2_log, "Worker terminating: ~p", [Reason]),
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) ->
%%                                     {ok, NewState} |
%%                                     {error, Reason}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%%%===================================================================
%%% Internal functions
%%%===================================================================

uniform_excluding(Range, ExcludeList) when is_list(ExcludeList) ->
    Exclude = sets:from_list(ExcludeList),
    uniform_excluding(Range, Exclude);
uniform_excluding(Range, ExcludeSet) ->
    Num = rand:uniform(Range),
    case sets:is_element(Num, ExcludeSet) of
        true -> uniform_excluding(Range, ExcludeSet);
        false -> Num
    end.

get_nth(_, 0, _) -> none;
get_nth(Key, Nth, IDs) ->
    Hash = erlang:phash2(Key, ?RING_SIZE),
    Primary = lists:foldl(fun(ID, Acc) ->
                                  if
                                      Acc > Hash -> Acc;
                                      ID > Hash -> ID;
                                      true -> Acc
                                  end
                          end,
                          hd(IDs),
                          tl(IDs)),
    % Rotate to primary head position
    Rot1 = fun Loop(IDList) ->
                               case hd(IDList) of
                                   Primary -> IDList;
                                   _ -> Loop(tl(IDList) ++ [hd(IDList)])
                               end
                       end(IDs),

    % Rotate to nth position
    Rot2 = fun Loop(1, IDList) -> IDList;
               Loop(N, IDList) -> Loop(N - 1, tl(IDList) ++ [hd(IDList)])
    end(Nth, Rot1),
    hd(Rot2).

filter_live(Workers) ->
    maps:filter(fun(_, #worker{status=Stat}) ->
                        case Stat of
                            alive -> true;
                            _ -> false
                        end
                end,
                Workers).

do_insert(Key, Value, From,
          State=#state{id=ID, workers=Workers, kvs=KVs}) ->
%    erlogger:log(kv2_log, "Doing insert for: ~p", [From]),
    TargetID = get_nth(Key, 1, lists:sort(maps:keys(filter_live(Workers)))),
    if
        TargetID == ID ->
            erlogger:log(kv2_log, "Inserting primary for ~p:~p",
                       [Key, Value]),
            case From of
                none -> ok;
                _ -> gen_server:reply(From, ok)
            end,
            {noreply, State#state{kvs=maps:put(Key, Value, KVs)}};
        true ->
            #worker{pid=WID} = maps:get(TargetID, Workers),
            gen_server:cast(WID, {insert, Key, Value, From}),
            NewStateList = lists:map(fun(X) ->
                                             {noreply, NewState} =
                                                 do_backup(Key, Value, X, State),
                                             NewState
                                     end,
                                     lists:seq(2, ?BACKUP_COUNT)),
            NewState = lists:foldl(fun(S=#state{backup=B1}, Acc=#state{backup=B2}) ->
                                           Acc#state{backup=maps:merge(B1, B2)}
                                   end,
                                   State,
                                   NewStateList),
            {noreply, NewState}
    end.

do_backup(Key, Value, Num,
          State=#state{id=ID, workers=Workers, backup=Backup}) ->
    %Primary = get_nth(Key, 1, lists:sort(maps:keys(filter_live(Workers)))),
    TargetID = get_nth(Key, Num, lists:sort(maps:keys(filter_live(Workers)))),
    if
        TargetID == ID ->
            erlogger:log(kv2_log, "Accepting backup ~p for ~p:~p",
                       [Num, Key, Value]),
            {noreply, State#state{backup=maps:put(Key, {Num, Value}, Backup)}};
        true ->
            #worker{pid=WID} = maps:get(TargetID, Workers),
            gen_server:cast(WID, {backup, Key, Value, Num}),
            {noreply, State}
    end.

do_lookup(Key, From,
          State=#state{id=ID, workers=Workers, kvs=KVs}) ->
    TargetID = get_nth(Key, 1, lists:sort(maps:keys(Workers))),
    if
        TargetID == ID ->
            gen_server:reply(From, maps:get(Key, KVs, undefined)),
            {noreply, State};
        true ->
            #worker{status=Status, pid=WID} = maps:get(TargetID, Workers),
            case Status of
                alive ->
                    gen_server:cast(WID, {lookup, Key, From});
                _ ->
                    do_backup_lookup(Key, From, State)
            end,
            {noreply, State}
    end.

do_backup_lookup(Key, From,
                 State=#state{workers=Workers, backup=Backup}) ->
    erlogger:log(kv2_log, "Getting backup for ~p", [Key]),
    IDs = lists:sort(maps:keys(Workers)),
    Backup_Workers =
        lists:map(fun(N) ->
                          maps:get(get_nth(Key, N, IDs), Workers)
                  end,
                  lists:seq(2, ?BACKUP_COUNT)),
    Live_Backups =
        lists:filter(fun(#worker{status=Stat}) ->
                             case Stat of
                                 alive -> true;
                                 _ -> false
                             end
                     end,
                     Backup_Workers),
    if
        length(Live_Backups) > 0 ->
            #worker{pid=Chosen} =
                lists:foldl(fun(W=#worker{pid=PID}, Acc) ->
                                    if
                                        PID == self() -> W;
                                        true -> Acc
                                    end
                            end,
                            hd(Live_Backups),
                            tl(Live_Backups)),
            if
                Chosen == self() ->
                    {_, Val} = maps:get(Key, Backup),
                    gen_server:reply(From, Val);
                true ->
                    gen_server:cast(Chosen, {backup_lookup, Key, From})
            end;
        true ->
            erlogger:log(kv2_log, "No live backups for: ~p~n~p",
                       [Key, Backup_Workers]),
            gen_server:reply(From, lost)
    end,
    {noreply, State}.
