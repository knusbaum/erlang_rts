%%%-------------------------------------------------------------------
%% @doc kv2_worker_sup supervisor.
%% @end
%%%-------------------------------------------------------------------

-module('kv2_worker_sup').
-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% RestartStrategy = one_for_all | one_for_one | rest_for_one | simple_one_for_one
%% MaxRestart = integer() >= 1
%% MaxTime = integer() >= 1
%% 
%% Child :: #{id => atom(),
%%            start => {M, F, A},
%%            restart => permanent | transient | temporary,
%%            shutdown => brutal_kill | timeout(),
%%            type => worker | supervisor, 
%%            modules => [atom()]}
init([]) ->
    {ok, {#{strategy => simple_one_for_one, 
            intensity => 1, 
            period => 30}, 
          [#{id => kv2_worker, 
             start => {kv2_worker, start_link, []}, 
             restart => temporary, 
             shutdown => 5000, 
             type => worker, 
             modules => [kv2_worker]}]}}.



%%====================================================================
%% Internal functions
%%====================================================================
