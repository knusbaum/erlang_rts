-module(kv2_server).
-behavior(gen_leader).

%% API
-export([start_link/1, start_link/2]).

%% gen_leader callbacks
-export([init/1,
         handle_cast/3,
         handle_call/4,
         handle_info/3,
         handle_leader_call/4,
         handle_leader_cast/3,
         handle_DOWN/3,
         elected/3,
         surrendered/3,
         from_leader/3,
         code_change/4,
         terminate/2]).

-define(SERVER, kv2_server).

-include("kv2_common.hrl").

%%--------------------------------------------------------------------
%% @doc
%% workers -> Map<Id -> #worker>
%% @end
%%--------------------------------------------------------------------
-record(state, {workers=#{}
                ,dead=#{}}).
                 
                 

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(Nodes) ->
    start_link(Nodes, []).

start_link(Nodes, Seed) when is_list(Nodes), is_atom(Seed) ->
    start_link(Nodes, {seed_node, Seed});

start_link(Nodes, Opts) ->
    gen_leader:start_link(?SERVER, Nodes, Opts, ?MODULE, [], []).

%%%===================================================================
%%% gen_leader callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    process_flag(trap_exit, true),
    ok = erlogger:start_logger(kv2_log, "kv2.log"),
    erlogger:log(kv2_log, "Doing Init."),
    error_logger:info_report(["Starting "]),
    gen_leader:cast(self(), init),
    {ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Called only in the leader process when it is elected. The Synch
%% term will be broadcasted to all the nodes in the cluster.
%%
%% @spec elected(State, Election, undefined) -> {ok, Synch, State}
%% @end
%%--------------------------------------------------------------------
elected(State, _Election, undefined) ->
    erlogger:log(kv2_log, "Elected as leader.", []),
    {ok, [], State};

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Called only in the leader process when a new candidate joins the
%% cluster. The Synch term will be sent to Node.
%%
%% @spec elected(State, Election, Node) -> {ok, Synch, State}
%% @end
%%--------------------------------------------------------------------
elected(State, _Election, Node) ->
    erlogger:log(kv2_log, "Node: ~p joined.", [Node]),
    {reply, [], State}.


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Called in all members of the cluster except the leader. Synch is a
%% term returned by the leader in the elected/3 callback.
%%
%% @spec surrendered(State, Synch, Election) -> {ok, State}
%% @end
%%--------------------------------------------------------------------
surrendered(State, Synch, _Election) ->
    erlogger:log(kv2_log, "Not the leader. Got: ~p", [Synch]),
    {ok, State}.


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages. Called in the leader.
%%
%% @spec handle_leader_call(Request, From, State, Election) ->
%%                                            {reply, Reply, Broadcast, State} |
%%                                            {reply, Reply, State} |
%%                                            {noreply, State} |
%%                                            {stop, Reason, Reply, State} |
%%                                            {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_leader_call({subscribe, ID, PID}, 
                   _From,
                   State=#state{workers=Workers}, 
                   _Election) ->
    case maps:get(ID, Workers, none) of
        none ->
            erlogger:log(kv2_log, "Worker ~p[~p] subscribed", [PID, ID]),
            Ref = monitor(process, PID),
            NewWorkers = maps:put(ID, #worker{status=alive, pid=PID, ref=Ref}, 
                                  Workers),
            %% Got new worker. Broadcast the info.
            %send_updated_worker_map(Workers, NewWorkers),
            erlogger:log(kv2_log, "Sending join:~n~p", [NewWorkers]),
            cast_workers(Workers, {join, NewWorkers}),
            {reply, 
             {ok, NewWorkers}, 
             State#state{workers=NewWorkers}};
        Worker -> 
            if 
                % Worker subscribed with existing ID.
                Worker == PID -> 
                    {reply, {ok, Workers}, State};
                true ->
                    erlogger:log(kv2_log, "Worker ~p[~p] id conflicts with ~p[~p].",
                               [PID, ID, Worker, ID]),
                    {reply, {id_in_use, Workers}, State}
            end
    end;
handle_leader_call(print_workers, _From, State=#state{workers=Workers}, _Election) ->
    Maps = call_workers(Workers, get_workers),
    {reply, Maps, State};
handle_leader_call(stop, _From, State, _Election) ->
    erlogger:log(kv2_log, "Got STOP.", []),
    {stop, normal, ok, State};
handle_leader_call(Msg, _From, State, _Election) ->
    erlogger:log(kv2_log, "Ignoring leader_call: ~p", [Msg]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages. Called in the leader.
%%
%% @spec handle_leader_cast(Request, State, Election) ->
%%                                            {ok, Broadcast, State} |
%%                                            {noreply, State} |
%%                                            {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_leader_cast(Request, State, _Election) ->
    erlogger:log(kv2_log, "Ignoring leader_cast: ~p", [Request]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling messages from leader.
%%
%% @spec from_leader(Request, State, Election) ->
%%                                    {ok, State} |
%%                                    {noreply, State} |
%%                                    {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
from_leader(Message, State, _Election) ->
    erlogger:log(kv2_log, "Got Message from leader: ~p", [Message]),
    {ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling nodes going down. Called in the leader only.
%%
%% @spec handle_DOWN(Node, State, Election) ->
%%                                  {ok, State} |
%%                                  {ok, Broadcast, State} |
%% @end
%%--------------------------------------------------------------------
handle_DOWN(Node, State, _Election) ->
    %% LEADER getting down node message
    erlogger:log(kv2_log, "Node went down: ~p", [Node]),
    {ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State, Election) ->
%%                                   {reply, Reply, State} |
%%                                   {noreply, State} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call({insert, Key, Value}, From, State, _Election) ->
    Workers = supervisor:which_children(kv2_worker_sup),
    {_,PID,_,_} = lists:nth(rand:uniform(length(Workers)), Workers),
    gen_server:cast(PID, {insert, Key, Value, From}),
    {noreply, State};
handle_call({lookup, Key}, From, State, _Election) ->
    Workers = supervisor:which_children(kv2_worker_sup),
    {_,PID,_,_} = lists:nth(rand:uniform(length(Workers)), Workers),
    gen_server:cast(PID, {lookup, Key, From}),
    {noreply, State};
handle_call(Request, _From, State, _Election) ->
    erlogger:log(kv2_log, "Ignoring call: ~p", [Request]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State, Election) ->
%%                                  {noreply, State} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(init, State, _Election) ->
    erlogger:log(kv2_log, "Got init. Launching workers."),
    launch_workers(),
    {noreply, State};
handle_cast(Msg, State, _Election) ->
    erlogger:log(kv2_log, "Ignoring Cast: ~p", [Msg]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State, Election) ->
%%                                   {noreply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info({'EXIT', Pid, Reason}, State, _Election) ->
    erlogger:log(kv2_log, "kv2_server got shutdown: [Reason: ~p from ~p]",
               [Reason, Pid]),
    {stop, Reason, State};
handle_info({'DOWN', Ref, process, Pid, Reason}, 
            State=#state{workers=Workers}, 
            _Election) ->
    erlogger:log(kv2_log, "kv2_server got monitor down: [~p, ~p] Reason: [~p]", 
               [Pid, Ref, Reason]),
    NewWorkers = mark_workers_down(Ref, Workers),
    %send_updated_worker_map(NewWorkers, NewWorkers),
    erlogger:log(kv2_log, "Sending part:~n~p", [NewWorkers]),
    cast_workers(Workers, {part, NewWorkers}),
    {noreply, State#state{workers=NewWorkers}};
handle_info({ldr, _, _, _, _, _}, State, _Election) ->
    % Extraneous election messages after election finished. Ignore entirely.
    {noreply, State};
handle_info(Message, State, _Election) ->
    erlogger:log(kv2_log, "kv2_server got unexpected message: ~p",
               [Message]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_leader when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_leader terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(shutdown, _State) ->
    erlogger:log(kv2_log, "Shutting down normally.", []),
    erlogger:stop_logger(kv2_log),
    ok;
terminate(Reason, _State) ->
    erlogger:log(kv2_log, "Shutting down ABNORMALLY! ~p", [Reason]),
    erlogger:stop_logger(kv2_log),
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Election, Extra) ->
%%                                          {ok, NewState} |
%%                                          {ok, NewState, NewElection}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, Election, _Extra) ->
    erlogger:log(kv2_log, "Election is: ~p", [Election]),
    {ok, State, Election}.


%%%===================================================================
%%% Internal functions
%%%===================================================================

cast_workers(Workers, Request) ->
    lists:map(fun(#worker{status=Stat, pid=PID}) ->
                      case Stat of
                          alive ->
                              gen_server:cast(PID, Request);
                          _ -> not_alive
                      end
              end,
              maps:values(Workers)).

call_workers(Workers, Request) ->
    lists:map(fun(#worker{status=Stat, pid=PID}) ->
                      case Stat of
                          alive ->
                              gen_server:call(PID, Request);
                          _ -> not_alive
                      end
              end,
              maps:values(Workers)).

mark_workers_down(DRef, Workers) ->
    maps:map(fun(_, W=#worker{ref=Ref}) ->
                     if 
                         DRef == Ref ->
                             W#worker{status=down};
                         true ->
                             W
                     end
             end,
             Workers).

launch_workers() ->
    WorkerCount = case application:get_env(kv2, worker_count) of
                      undefined -> 4;
                      {ok, Count} -> Count
                  end,
    fun Loop(0) -> ok;
        Loop(I) -> {ok, _} = supervisor:start_child(kv2_worker_sup, []),
                   Loop(I - 1)
                 end(WorkerCount).

