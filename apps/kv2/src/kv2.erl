%%%-------------------------------------------------------------------
%% @doc kv2 public API
%% @end
%%%-------------------------------------------------------------------

-module('kv2').

-behaviour(application).

%% Application callbacks
-export([start/2
         ,stop/1
         ,dump_worker_maps/0
         ,insert/2
         ,lookup/1
         ,test/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    kv2_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%--------------------------------------------------------------------
dump_worker_maps() ->
    gen_leader:leader_call(kv2_server, print_workers).

%%--------------------------------------------------------------------
insert(Key, Value) ->
    gen_leader:call(kv2_server, {insert, Key, Value}).

lookup(Key) ->
    gen_leader:call(kv2_server, {lookup, Key}).

test(0) -> ok;
test(Count) ->
    insert(Count, Count),
    test(Count - 1).

%%====================================================================
%% Internal functions
%%====================================================================
