-module(echo_serv).
-behavior(gen_server).
-export([start_link/0, init/1, handle_call/3, handle_cast/2,
         handle_info/2, terminate/2, code_change/3, accept/2]).

start_link() ->
    gen_server:start_link({local, echo}, ?MODULE, [], []).

init([]) ->
    process_flag(trap_exit, true),
    ok = erlogger:start_logger(echo_log, "echo.log"),
    self() ! init,
    {ok, {noaccept, nosock, []}}.

handle_call({connect, Sock}, _From, {Accept, LSock, SockList}) ->
    erlogger:log(echo_log, "Got Connection.", []),
    inet:setopts(Sock, [{active, true}]),
    {reply, ok, {Accept, LSock, [Sock | SockList]}};
handle_call(_,_,State) ->
    {noreply, State}.

handle_cast(_, State) ->
    {noreply, State}.

accept(LSock, Pid) ->
    erlogger:log(echo_log, "Starting Accept loop.", []),
    {ok, Sock} = gen_tcp:accept(LSock),
    gen_tcp:controlling_process(Sock, Pid),
    gen_server:call(Pid,{connect, Sock}),
    accept(LSock, Pid).

handle_info({'EXIT', Pid, Reason}, {Accept, LSock, SockList}) ->
    if Pid == Accept -> 
            erlogger:log(echo_log, "Accepter died. Launching new one.", []),
            NewAccept = spawn_link(?MODULE, accept, [LSock, self()]),
            {noreply, {NewAccept, LSock, SockList}};
       true ->
            erlogger:log(echo_log, "Echo Server got shutdown: [Reason: ~p, From: ~p]",
                       [Reason, Pid]),
            {stop, Reason, {Accept, LSock, SockList}}
    end;
handle_info(init, {noaccept, nosock, []}) ->
    erlogger:log(echo_log, "Starting Listener.", []),
    {ok, Port} = application:get_env(port),
    erlogger:log(echo_log, "Starting on port ~p", [Port]),
    {ok, LSock} = gen_tcp:listen(Port, [binary, {active, false}]),
    Accept = spawn_link(?MODULE, accept, [LSock, self()]),
    {noreply, {Accept, LSock, []}};
handle_info({tcp, _Port, Data}, {Accept, LSock, SockList}) ->
    erlogger:log(echo_log, "Got Message: ~p", [Data]),
    [gen_tcp:send(X, Data) || X <- SockList],
    {noreply, {Accept, LSock, SockList}};
handle_info({tcp_closed, Port}, {Accept, LSock, SockList}) ->
    erlogger:log(echo_log, "Port Closed: ~p | ~p", [Port, SockList]),
    Res = lists:delete(Port, SockList),
    erlogger:log(echo_log, "Next: ~p", [Res]),
    {noreply, {Accept, LSock, Res}};
handle_info(Message, State) ->
    erlogger:log(echo_log, "Echo Server got unexpected message: ~p",
               [Message]),
    {noreply, State}.



terminate(shutdown, {_Accept, LSock, SockList}) ->
    erlogger:log(echo_log, "Echo Server shutting down normally.", []),
    case LSock of
        nosock -> ok;
        _ -> gen_tcp:close(LSock)
    end,
    [gen_tcp:close(X) || X <- SockList],
    erlogger:stop_logger(echo_log),
    ok;
terminate(Reason, {_Accept, LSock, SockList}) ->
    erlogger:log(echo_log, "Echo Server shutting down abnormally: ~p", [Reason]),
    case LSock of
        nosock -> ok;
        _ -> gen_tcp:close(LSock)
    end,
    [gen_tcp:close(X) || X <- SockList],
    erlogger:stop_logger(echo_log),
    ok.

code_change(_,State,_) ->
    {ok, State}.
