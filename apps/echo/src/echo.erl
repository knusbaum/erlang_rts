-module(echo).
-behavior(application).
-export([start/2, stop/1]).

start(normal, _Args) ->
    supervisor:start_link(echo_supervisor, []).

stop(_) ->
    ok.
