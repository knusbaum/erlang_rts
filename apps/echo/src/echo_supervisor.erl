-module(echo_supervisor).
-behavior(supervisor).
-export([init/1]).

init([]) ->
    {ok, {{one_for_one, 5, 10},
          [{echo_serv,
            {echo_serv, start_link, []},
            permanent,
            10000,
            worker,
            [echo_serv]}]}}.
