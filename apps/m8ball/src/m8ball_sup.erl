%%%-------------------------------------------------------------------
%% @doc m8ball top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module('m8ball_sup').

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    Nodes = case application:get_env(m8ball, nodes) of
                undefined -> [node()];
                {ok, NodeList} -> sets:to_list(sets:from_list([node() | NodeList]))
            end,
    {ok, {{one_for_all, 1, 10}, 
          [{m8ball,
            {m8ball_server, start_link, [Nodes]},
            permanent,
            5000,
            worker,
            [m8ball_server]
           }]}}.

%%====================================================================
%% Internal functions
%%====================================================================
