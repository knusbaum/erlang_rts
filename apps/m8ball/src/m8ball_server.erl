-module(m8ball_server).
-behavior(gen_leader).

%% API
-export([start_link/1, start_link/2]).

%% gen_leader callbacks
-export([init/1,
         handle_cast/3,
         handle_call/4,
         handle_info/3,
         handle_leader_call/4,
         handle_leader_cast/3,
         handle_DOWN/3,
         elected/3,
         surrendered/3,
         from_leader/3,
         code_change/4,
         terminate/2]).

-define(SERVER, m8ball).

-record(state, {answers = [<<"Sure thing!">>,
                           <<"No way!!!">>,
                           <<"Who's to say?">>,
                           <<"Don't count on it.">>,
                           <<"All signs point to yes.">>]}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(Nodes) ->
    start_link(Nodes, []).

start_link(Nodes, Seed) when is_list(Nodes), is_atom(Seed) ->
    start_link(Nodes, {seed_node, Seed});

start_link(Nodes, Opts) ->
    gen_leader:start_link(?SERVER, Nodes, Opts, ?MODULE, [], []).

%%%===================================================================
%%% gen_leader callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    process_flag(trap_exit, true),
    io:format("TRYING TO START m8ball logger!~n"),
    ok = erlogger:start_logger(m8ball_log, "m8ball.log"),
    io:format("Done. error Logging m8ball!~n"),
    error_logger:info_report(["Starting m8ball"]),
    io:format("Done!~n"),
    {ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Called only in the leader process when it is elected. The Synch
%% term will be broadcasted to all the nodes in the cluster.
%%
%% @spec elected(State, Election, undefined) -> {ok, Synch, State}
%% @end
%%--------------------------------------------------------------------
elected(State, _Election, undefined) ->
    erlogger:log(m8ball_log, "Elected as leader.", []),
    Synch = [],
    {ok, Synch, State};

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Called only in the leader process when a new candidate joins the
%% cluster. The Synch term will be sent to Node.
%%
%% @spec elected(State, Election, Node) -> {ok, Synch, State}
%% @end
%%--------------------------------------------------------------------
elected(State, _Election, Node) ->
    erlogger:log(m8ball_log, "Node: ~p joined.", [Node]),
    {reply, [], State}.


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Called in all members of the cluster except the leader. Synch is a
%% term returned by the leader in the elected/3 callback.
%%
%% @spec surrendered(State, Synch, Election) -> {ok, State}
%% @end
%%--------------------------------------------------------------------
surrendered(State, Synch, _Eelection) ->
    erlogger:log(m8ball_log, "Not the leader. Got: ~p", [Synch]),
    {ok, State}.


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages. Called in the leader.
%%
%% @spec handle_leader_call(Request, From, State, Election) ->
%%                                            {reply, Reply, Broadcast, State} |
%%                                            {reply, Reply, State} |
%%                                            {noreply, State} |
%%                                            {stop, Reason, Reply, State} |
%%                                            {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_leader_call({question, die}, _From, State, _Election) ->
    erlogger:log(m8ball_log, "Got BAD question.", []),
    X = 10 - 5 - 5,
    {reply, {<<"No answers at this time.">>, 5/X, self()}, State};
handle_leader_call({question, _Q}, _From, State = #state{answers = Answers}, _Election) ->
    Answer = lists:nth(random:uniform(length(Answers)), Answers),
    erlogger:log(m8ball_log, "Got question.", []),
    {reply, {Answer, self()}, State};
%    {reply, {<<"No answers at this time.">>, self()}, State};
handle_leader_call(stop, _From, State, _Election) ->
    erlogger:log(m8ball_log, "Got STOP.", []),
    {stop, normal, ok, State};
handle_leader_call(_, _From, State, _Election) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages. Called in the leader.
%%
%% @spec handle_leader_cast(Request, State, Election) ->
%%                                            {ok, Broadcast, State} |
%%                                            {noreply, State} |
%%                                            {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_leader_cast(_Request, State, _Election) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling messages from leader.
%%
%% @spec from_leader(Request, State, Election) ->
%%                                    {ok, State} |
%%                                    {noreply, State} |
%%                                    {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
from_leader(Synch, State, _Election) ->
    erlogger:log(m8ball_log, "Got Synch from leader: ~p", [Synch]),
    {ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling nodes going down. Called in the leader only.
%%
%% @spec handle_DOWN(Node, State, Election) ->
%%                                  {ok, State} |
%%                                  {ok, Broadcast, State} |
%% @end
%%--------------------------------------------------------------------
handle_DOWN(Node, State, _Election) ->
    erlogger:log(m8ball_log, "Node went down: ~p", [Node]),
    {ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State, Election) ->
%%                                   {reply, Reply, State} |
%%                                   {noreply, State} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call(Request, _From, State, _Election) ->
    erlogger:log(m8ball_log, "Handling call: ~p", [Request]),
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State, Election) ->
%%                                  {noreply, State} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(_Msg, State, _Election) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State, Election) ->
%%                                   {noreply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info({'EXIT', Pid, Reason}, State, _Election) ->
    erlogger:log(m8ball_log, "Magic 8 Ball server got shutdown: [Reason: ~p from ~p]",
               [Reason, Pid]),
    {stop, Reason, State};
handle_info({ldr, _, _, _, _, _}, State, _Election) ->
    % Extraneous election messages after election finished. Ignore entirely.
    {noreply, State};
handle_info(Message, State, _Election) ->
    erlogger:log(m8ball_log, "Magic 8 Ball got unexpected message: ~p",
               [Message]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_leader when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_leader terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(shutdown, State) ->
    erlogger:log(m8ball_log, "Magic 8 Ball server shutting down normally.", []),
    erlogger:stop_logger(m8ball_log),
    ok;
terminate(Reason, State) ->
    erlogger:log(m8ball_log, "Magic 8 Ball server shutting down ABNORMALLY! ~p", [Reason]),
    erlogger:stop_logger(m8ball_log),
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Election, Extra) ->
%%                                          {ok, NewState} |
%%                                          {ok, NewState, NewElection}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, Election, _Extra) ->
    erlogger:log(m8ball_log, "Election is: ~p", [Election]),
    NewState = #state{},
    {ok, State#state{answers=NewState#state.answers}, Election}.


%%%===================================================================
%%% Internal functions
%%%===================================================================
